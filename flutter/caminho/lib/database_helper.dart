import 'dart:io';
import 'dart:async';
import 'dart:typed_data';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:flutter/services.dart';

import 'model/book.dart';
import 'model/chapter.dart';
import 'model/point.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;

  static Database _db;

  Future<Database> get db async {
    if (_db != null)
      return _db;
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "asset_database.db");
    // Only copy if the database doesn't exist
    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound){
      // Load database from asset and copy
      ByteData data = await rootBundle.load(join('assets', 'database.db'));
      List<int> bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

      // Save copied asset to documents
      await new File(path).writeAsBytes(bytes);
    }

    var db = await openDatabase(path);
//     var db = await openDatabase(join('assets', 'database.db'));
    return db;
  }

  //Books
  Future<List<Book>> getAllBooks() async {
    var dbClient = await db;
    var res = await dbClient.query("books");
    List<Book> list = res.isNotEmpty ? res.map((c) => Book.fromMap(c)).toList() : null;
    return list;
  }

//   Future<Book> getBook(int id) async {
//     var res = await _db.rawQuery('SELECT * FROM _books WHERE _id = $id');
//     Book book = res.isNotEmpty ? new Book.fromMap(res.first) : new Book(0,'Zero');
//     return book; 
//   }

  //Chapters
  Future<List<Chapter>> getBookChapters(int book) async {
    var dbClient = await db;
    var res = await dbClient.rawQuery('SELECT * FROM chapters WHERE _book = $book');
    List<Chapter> list = res.isNotEmpty ? res.map((c) => Chapter.fromMap(c)).toList() : null;
    return list;
  }

  //Points
  Future<List<Point>> getBookChapterPoints(int book, int chapter) async {
    var dbClient = await db;
    var res = await dbClient.rawQuery('SELECT * FROM points p, chapter_points cp WHERE p._book = $book and cp._chapter = $chapter and p._id = cp._id');
    List<Point> list = res.isNotEmpty ? res.map((c) => Point.fromMap(c)).toList() : null;
    return list;
  }

  Future close() async {
    return _db.close();
  }
}