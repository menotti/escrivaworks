// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// A simple "rough and ready" example of localizing a Flutter app.
// Spanish and English (locale language codes 'en' and 'es') are
// supported.

// The pubspec.yaml file must include flutter_localizations in its
// dependencies section. For example:
//
// dependencies:
//   flutter:
//   sdk: flutter
//  flutter_localizations:
//    sdk: flutter

// If you run this app with the device's locale set to anything but
// English or Spanish, the app's locale will be English. If you
// set the device's locale to Spanish, the app's locale will be
// Spanish.

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter_localizations/flutter_localizations.dart';

import 'database_helper.dart'; 
import 'model/book.dart';
import 'model/chapter.dart';
import 'model/point.dart';

class CL {
  CL(this.locale);

  final Locale locale;

  static CL of(BuildContext context) {
    return Localizations.of<CL>(context, CL);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'de': {
      'books': 'Bücher',
      'chapters': 'Kapitel',
      'favorites': 'Favoriten',
      'point': 'Punkt...', 
      'search': 'Suche', 
      'title': 'Der Weg', 
    },
    'en': {
      'books': 'Books', 
      'chapters': 'Chapters',
      'favorites': 'Favorites', 
      'point': 'Point...', 
      'search': 'Search', 
      'title': 'The Way', 
    },
    'es': {
      'books': 'Libros', 
      'chapters': 'Capítulos',
      'favorites': 'Favoritos', 
      'point': 'Punto...', 
      'search': 'Buscar', 
      'title': 'Camino', 
    },
    'fr': {
      'books': 'Livres', 
      'chapters': 'Chapitres',
      'favorites': 'Favoris', 
      'point': 'Point...', 
      'search': 'Chercher', 
      'title': 'Chemin', 
    },
    'pt': {
      'books': 'Livros', 
      'chapters': 'Capítulos',
      'favorites': 'Favoritos', 
      'point': 'Ponto...', 
      'search': 'Buscar', 
      'title': 'Caminho', 
    },
  };

  String str(String str) {
    return _localizedValues[locale.languageCode][str];
  }
}

class CLDelegate extends LocalizationsDelegate<CL> {
  const CLDelegate();

  @override
  bool isSupported(Locale locale) => ['de', 'en', 'es', 'fr', 'pt'].contains(locale.languageCode);

  @override
  Future<CL> load(Locale locale) {
    // Returning a SynchronousFuture here because an async "load" operation
    // isn't needed to produce an instance of CL.
    return SynchronousFuture<CL>(CL(locale));
  }

  @override
  bool shouldReload(CLDelegate old) => false;
}


class Points extends StatefulWidget {
   int book;
   int chapter;
   String title;
   Points(
     this.book, 
     this.chapter,
     this.title
   );
  _Points createState() => _Points(book, chapter, title);
}

class _Points extends State<Points> {
  int book; 
  int chapter;
  String title;
  DatabaseHelper db = DatabaseHelper(); 

  _Points(
     this.book, 
     this.chapter,
     this.title
  );
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),      
      body: FutureBuilder<List<Point>>(
        future: db.getBookChapterPoints(book, chapter), 
        builder: (context, snapshot) {
          return ListView(
            children: snapshot.data
                .map((point) => ListTile(
                      title: Text(point.text),
                    ))
                .toList(),
          );
        },
      ),      
    );
  }
}

class Chapters extends StatefulWidget {
   int book;
   Chapters(
     this.book 
   );
  _Chapters createState() => _Chapters(book);
}

class _Chapters extends State<Chapters> {
  int book; 
  DatabaseHelper db = DatabaseHelper(); 

  _Chapters(
    this.book 
  );
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(CL.of(context).str('chapters')),
      ),      
      body: FutureBuilder<List<Chapter>>(
        future: db.getBookChapters(book), 
        builder: (context, snapshot) {
          return ListView(
            children: snapshot.data
                .map((chapter) => ListTile(
                      title: Text('${chapter.id} - ${chapter.title}'),
                      onTap: () {
                        Navigator.push(
                          context, 
                          MaterialPageRoute(
                            builder: (context) => Points(book, chapter.id, '${chapter.id} - ${chapter.title}')
                          ),
                        );
                      },
                    ))
                .toList(),
          );
        },
      ),      
    );
  }
}

class Books extends StatefulWidget {
  _Books createState() => _Books();
}

class _Books extends State<Books> {
  DatabaseHelper db = DatabaseHelper(); 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(CL.of(context).str('books')),
      ),      
      body: FutureBuilder<List<Book>>(
        future: db.getAllBooks(),
        builder: (context, snapshot) {
          return ListView(
            children: snapshot.data
                .map((book) => ListTile(
                      title: Text(book.title),
                      onTap: () {
                        Navigator.push(
                          context, 
                          MaterialPageRoute(
                            builder: (context) => Chapters(book.id)
                          ),
                        );
                      },
                    ))
                .toList(),
          );
        },
      ),      
    );
  }
}

class CaminoApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(CL.of(context).str('title')),
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            title: Text(CL.of(context).str('books')),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => Books()),);
            },
          ),
          ListTile(
            title: Text(CL.of(context).str('search')),
          ),
          ListTile(
            title: Text(CL.of(context).str('favorites')),
          ),
          ListTile(
            title: Text(CL.of(context).str('point')),
            onLongPress: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => Books()),);
            },
          ),
        ]
      ),
    );
  }
}

class Camino extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateTitle: (BuildContext context) => CL.of(context).str('title'),
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
        const CLDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('de', ''),
        const Locale('en', ''),
        const Locale('es', ''),
        const Locale('fr', ''),
        const Locale('pt', ''),
      ],
      theme: new ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: CaminoApp(),
    );
  }
}

void main() {
  runApp(Camino());
}
