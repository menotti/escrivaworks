class Book {
  int id;
  String title;

  Book(
    this.id, 
    this.title
  );

  Book.fromMap(dynamic obj) {
    this.id = obj['_book']; // _id 
    this.title = obj['_title'];
  }

//   Map<String, dynamic> toMap() {
//     var map = new Map<String, dynamic>();
//     map['_id'] = id;
//     map['_title'] = title;
//     return map;
//   }
  
  int get getId => id;
  String get getTitle => title;
}
