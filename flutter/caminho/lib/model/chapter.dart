class Chapter {
  int id;
  String title;

  Chapter(
    this.id, 
    this.title
  );

  Chapter.fromMap(dynamic obj) {
    this.id = obj['_chapter']; // _id
    this.title = obj['_title'];
  }

//   Map<String, dynamic> toMap() {
//     var map = new Map<String, dynamic>();
//     map['_id'] = id;
//     map['_title'] = title;
//     return map;
//   }
  
  int get getId => id;
  String get getTitle => title;
}
