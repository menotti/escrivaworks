class Point {
  int id;
  String text;

  Point(
    this.id, 
    this.text
  );

  Point.fromMap(dynamic obj) {
    this.id = obj['_id']; 
    this.text = obj['_text'];
  }

//   Map<String, dynamic> toMap() {
//     var map = new Map<String, dynamic>();
//     map['_id'] = id;
//     map['_title'] = title;
//     return map;
//   }
  
  int get getId => id;
  String get getText => text;
}
