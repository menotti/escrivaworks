//
//  CapitulosViewController.swift
//  Caminho
//
//  Created by Jonathan Loureiro on 18/07/2018.
//  Copyright © 2018 escrivaworks. All rights reserved.
//

import UIKit

class CapitulosViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var numberOfRowsInSection: Int = 0
    
    struct id {
        static let cell = "chapterCell"
        static let segue = "textSegue"
    }
    
    var chapters: [Database.Chapter]! {
        didSet {
            self.numberOfRowsInSection = self.chapters.count
        }
    }

    var bookTitle: String! {
        didSet {
            self.navigationItem.title = bookTitle
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == id.segue {
            let indexPath = self.tableView.indexPathForSelectedRow!
            let controller = segue.destination as! TextoViewController
            controller.chapterPoints = chapters[indexPath.row].chapterPoints
            controller.chapterTitle = "\(self.chapters[indexPath.row].capitulo). \(self.chapters[indexPath.row].title)"
            controller.bookTitle = self.bookTitle
        }
    }

}

extension CapitulosViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id.cell, for: indexPath)
        cell.textLabel?.text = "\(self.chapters[indexPath.row].capitulo). \(self.chapters[indexPath.row].title)"
        return cell
    }
    
}

extension CapitulosViewController: UITableViewDelegate {
    
}
