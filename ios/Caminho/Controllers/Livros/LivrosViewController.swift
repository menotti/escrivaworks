//
//  LivrosViewController.swift
//  Caminho
//
//  Created by Jonathan Loureiro on 18/07/2018.
//  Copyright © 2018 escrivaworks. All rights reserved.
//

import UIKit
import SQLite

class LivrosViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    fileprivate var numberOfRowsInSection: Int = 0
    fileprivate var connection: Connection?

    struct id {
        static let cell = "bookCell"
        static let segue = "chaptersSegue"
    }

    fileprivate var books: [Database.Book]! {
        didSet {
            self.numberOfRowsInSection = self.books.count
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.books = []
        self.setupBooks()
    }
    
    func setupBooks() {
        if let path = Database.Path {
            do {
                self.connection = try Connection(path)
                let books = Table(Database.Book.tableName)
                for row in try self.connection!.prepare(books) {
                    let book = Database.Book(self.connection!, row: row)
                    self.books.insert(book, at: Int(book.book))
                }
            } catch {}
        }
    }
    
    //MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == id.segue {
            let indexPath = self.tableView.indexPathForSelectedRow!
            let controller = segue.destination as! CapitulosViewController
            controller.bookTitle = self.books[indexPath.row].title
            controller.chapters = self.books[indexPath.row].chapters
        }
    }
    
}

extension LivrosViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id.cell, for: indexPath)
        cell.textLabel?.text = self.books[indexPath.row].title
        return cell
    }
    
    
}

extension LivrosViewController: UITableViewDelegate {}
