//
//  TextoViewController.swift
//  Caminho
//
//  Created by Jonathan Loureiro on 19/07/2018.
//  Copyright © 2018 escrivaworks. All rights reserved.
//

import UIKit

class TextoViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var numberOfRowsInSection: Int = 0
    var bookTitle: String!
    
    struct id {
        static let cell = "textCell"
    }
    
    var chapterPoints: [Database.ChapterPoints]! {
        didSet {
            self.numberOfRowsInSection = chapterPoints.count
        }
    }
    
    var chapterTitle: String! {
        didSet {
            self.navigationItem.title = chapterTitle
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}


extension TextoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id.cell, for: indexPath)
        cell.textLabel?.text = "\(self.bookTitle!) \(self.chapterPoints[indexPath.row].point): \(self.chapterPoints[indexPath.row].text)"
        return cell
    }
}

extension TextoViewController: UITableViewDelegate {
    
}
