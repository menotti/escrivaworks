//
//  Database.swift
//  Caminho
//
//  Created by Jonathan Loureiro on 18/07/2018.
//  Copyright © 2018 escrivaworks. All rights reserved.
//

import Foundation
import SQLite

class Database {
        
    struct Book {
        static let tableName: String = "books"
        static let _id = Expression<Int64>("_id")
        static let _language = Expression<Int64>("_language")
        static let _book = Expression<Int64>("_book")
        static let _title = Expression<String>("_title")
        static let _points = Expression<Int64>("_points")
        
        let id: Int64
        let language: Int64
        let book: Int64
        let title: String
        let points: Int64
        
        var connection: Connection
        
        lazy var chapters: [Database.Chapter] = {
            return self.setupChapters(book: self.book)
        }()
        
        init(_ connection: Connection,row: Row) {
            self.connection = connection
            self.id = row[Book._id]
            self.language = row[Book._language]
            self.book = row[Book._book]
            self.title = row[Book._title]
            self.points = row[Book._points]
        }
        
        func printBook() {
            print(self.id, self.language, self.book, self.title, self.points)
        }
        
        private func setupChapters(book: Int64) -> [Database.Chapter] {
            var chapters: [Database.Chapter] = [Database.Chapter]()
            let capter = Table(Database.Chapter.tableName)
            let filter: Expression<Bool> = (Database.Chapter._book == book)
            let query = capter.where(filter)
            do {
                for row in try self.connection.prepare(query) {
                    let chapter = Database.Chapter(connection, row: row)
                    chapters.insert(chapter, at: Int(chapter.capitulo))
                }
            } catch {}
            return chapters
        }
    }

    struct Chapter {
        static let tableName: String = "chapters"
        static let _id = Expression<Int64>("_id")
        static let _language = Expression<Int64>("_language")
        static let _book = Expression<Int64>("_book")
        static let _chapter = Expression<Int64>("_chapter")
        static let _title = Expression<String>("_title")
        
        let id: Int64
        let language: Int64
        let book: Int64
        let title: String
        let capitulo: Int64
        
        var connection: Connection
        
        lazy var chapterPoints: [Database.ChapterPoints] = {
            return self.setupChapterPoints(book: self.book, capitulo: self.capitulo)
        }()
        
        init(_ connection: Connection, row: Row) {
            self.connection = connection
            self.id = row[Chapter._id]
            self.language = row[Chapter._language]
            self.book = row[Chapter._book]
            self.title = row[Chapter._title]
            self.capitulo = row[Chapter._chapter]
        }
        
        private func setupChapterPoints(book: Int64, capitulo: Int64) -> [Database.ChapterPoints] {
            var chapterPoints: [Database.ChapterPoints] = [Database.ChapterPoints]()
            let chapter_points = Table(Database.ChapterPoints.tableName)
            let book: Expression<Bool> = (Database.Chapter._book == book)
            let filter: Expression<Bool> = (Database.Chapter._chapter == capitulo)
            let query = chapter_points.filter(book).filter(filter)
            do {
                for row in try self.connection.prepare(query) {
                    let chapterPoint = Database.ChapterPoints(self.connection, row: row)
                    chapterPoints.append(chapterPoint)
                }
            } catch {}
            return chapterPoints
        }
    }
    
    struct ChapterPoints {
        static let tableName: String = "chapter_points"
        static let _id = Expression<Int64>("_id")
        static let _language = Expression<Int64>("_language")
        static let _book = Expression<Int64>("_book")
        static let _chapter = Expression<Int64>("_chapter")
        static let _point = Expression<Int64>("_point")
        
        let id: Int64
        let language: Int64
        let book: Int64
        let chapter: Int64
        let point: Int64
        var text: String = ""
                
        init(_ connection: Connection, row: Row) {
            self.id = row[ChapterPoints._id]
            self.language = row[ChapterPoints._language]
            self.book = row[ChapterPoints._book]
            self.chapter = row[ChapterPoints._chapter]
            self.point = row[ChapterPoints._point]
            
            let point = Table(Database.Point.tableName)
            let book: Expression<Bool> = (Database.Point._book == row[ChapterPoints._book])
            let filter: Expression<Bool> = (Database.Point._point == row[ChapterPoints._point])
            let query = point.filter(book).filter(filter)
            do {
                for row in try connection.prepare(query.select(Database.Point._text)) {
                    self.text = row[Database.Point._text]
                    print(text)
                }
            } catch {}
        }
    }
    
    struct Point {
        static let tableName: String = "points"
        static let _id = Expression<Int64>("_id")
        static let _language = Expression<Int64>("_language")
        static let _book = Expression<Int64>("_book")
        static let _chapter = Expression<Int64>("_chapter")
        static let _point = Expression<Int64>("_point")
        static let _text = Expression<String>("_text")
    }

    struct Languages {
        static let _id = Expression<Int64>("_id")
        static let _language = Expression<Int64>("_language")
    }
    
    static let Path: String? = {
        return Bundle.main.path(forResource: "opus_pt", ofType: "db")
    }()
    
}
