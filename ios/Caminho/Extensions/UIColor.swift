//
//  UIColor.swift
//  Caminho
//
//  Created by Jonathan Loureiro on 19/07/2018.
//  Copyright © 2018 escrivaworks. All rights reserved.
//

import UIKit

extension UIColor {
    class func rgba(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat, _ alpha: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
    
    class func rgb(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat) -> UIColor {
        return UIColor.rgba(red, green, blue, 1)
    }
}
